module.exports = class salaDeAulaController {
// Método para cadastrar uma nova sala de aula
  static async cadastroSalaDeAula(req, res) {
    const { numeroSala, recurso, localizacao, capacidade } = req.body;
    // Verifica se todos os campos estão preenchidos
      if (numeroSala !== "" && recurso !== "" && localizacao !== "" && capacidade !== "") {
      // Verifica se a capacidade é um número inteiro positivo
      if (!isNaN(capacidade) && parseInt(capacidade) > 0) {
    return res.status(200).json({ message: "Sala cadastrada com sucesso!" });
      } else {
    return res.status(500).json({ message: "A capacidade deve ser um número inteiro positivo." });
      }//Fim do else
    } else {
    return res.status(500).json({ message: "Erro ao cadastrar sala, preencha todos os campos corretamente!" });
    }//Fim do else
  }//Fim do cadastro

// Método para atualizar uma sala de aula existente
  static async updateSalaDeAula(req, res) {
    const { numeroSala, recurso, localizacao, capacidade } = req.body;
    // Verifica se todos os campos estão preenchidos
      if (numeroSala !== "" && recurso !== "" && localizacao !== "" && capacidade !== "") {
      // Verifica se a capacidade é um número inteiro positivo
      if (!isNaN(capacidade) && parseInt(capacidade) > 0) {
    return res.status(200).json({ message: "Sala de aula atualizada com sucesso!" });
      } else {
    return res.status(500).json({ message: "A capacidade deve ser um número inteiro positivo." });
      }//Fim do else verificação
    } else {
    return res.status(500).json({ message: "Não foi possível atualizar sala de aula, preencha todos os campos corretamente!" });
    }//Fim do else
  }//Fim do update

  // Método para solicitar dados 
  static async getSalaDeAula(req, res) {
      const numeroSala = "B12";
      const capacidade = 33;
      const recurso = "Sala de Computadores";
      const localizacao = "Bloco A, 1º andar";
      return res.status(200).json({ numeroSala: numeroSala, capacidade: capacidade, recurso: recurso, localizacao: localizacao });
  }//Fim do get

// Método para deletar uma sala de aula
  static async deleteSalaDeAula(req, res) {
    try {
      const salaDeAulaId = req.params.id;
      return res.status(200).json({ message: "Sala de aula removida com sucesso!", salaDeAulaId: salaDeAulaId });
    } catch (error) {
      console.error("Erro ao excluir sala de aula!", error);
      return res.status(500).json({ message: "Ocorreu um erro ao tentar excluir!" });
    }//Fim do catch
  }//Fim do delete
};//Fim da class
